package net.therap.model;

import net.therap.service.validator.CollectionValueProvider;
import net.therap.service.validator.HeightValueProvider;
import net.therap.service.validator.IntegerValueProvider;
import net.therap.service.validator.StringValueProvider;
import java.util.*;

/**
 * @author al.imran
 * @since 23/03/2021
 */
public class Person {
    @Size(max = 10, valueProvider = StringValueProvider.class)
    private String name;

    @Size(min = 18, message = "Age must be {min}-{max}", valueProvider = IntegerValueProvider.class)
    private Integer age;

    @Size(max = 6, message = "Height must be {min}-{max}", valueProvider = HeightValueProvider.class)
    private Height height;

    @Size(max = 3, message = "List size must be {min}-{max}", valueProvider = CollectionValueProvider.class)
    private List<String> list;

    public Person(String name, int age, int height) {
        this.name = name;
        this.age = age;
        this.height = new Height(height);
        this.list = new ArrayList<>();
    }

    public void populateList() {
        list.add("Hello World");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
