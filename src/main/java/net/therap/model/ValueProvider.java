package net.therap.model;

/**
 * @author al.imran
 * @since 28/03/2021
 */
public interface ValueProvider<T> {
    int getSize(T object);
    String getType();
}
