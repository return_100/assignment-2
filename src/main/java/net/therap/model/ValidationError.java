package net.therap.model;

/**
 * @author al.imran
 * @since 23/03/2021
 */
public class ValidationError {
    private String fieldName;
    private String fieldType;
    private String errorMessage;

    public ValidationError() {
        this.fieldName = "";
        this.fieldType = "";
        this.errorMessage = "";
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
