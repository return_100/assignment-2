package net.therap.model;

/**
 * @author al.imran
 * @since 28/03/2021
 */
public class Height {
    private int height;

    public Height(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
