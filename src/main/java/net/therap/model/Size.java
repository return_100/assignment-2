package net.therap.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author al.imran
 * @since 23/03/2021
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Size {
    int min() default 1;
    int max() default 100;
    String message() default "Length must be {min}-{max}";
    Class<? extends ValueProvider> valueProvider();
}
