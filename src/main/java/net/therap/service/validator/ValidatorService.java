package net.therap.service.validator;

import net.therap.controller.ValidatorController;
import net.therap.model.Size;
import net.therap.model.ValidationError;
import net.therap.model.ValueProvider;
import net.therap.service.string.StringFormatService;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * @author al.imran
 * @since 23/03/2021
 */
public class ValidatorService {
    private static final String PATTERN = "\\{[a-zA-Z]+[a-zA-Z0-9]*\\}";
    private static final String MIN_STRING = "{min}";
    private static final String MAX_STRING = "{max}";
    private final List<ValidationError> errors;

    public ValidatorService() {
        this.errors = new ArrayList<>();
    }

    public void validate(Object object) {
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Size.class)) {
                Size annotation = field.getDeclaredAnnotation(Size.class);
                field.setAccessible(true);
                ValueProvider valueProvider = null;
                try {
                    valueProvider = (ValueProvider) annotation.valueProvider().getConstructor().newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                if (valueProvider != null) {
                    try {
                        int size = valueProvider.getSize(field.get(object));
                        if (size < annotation.min() || size > annotation.max()) {
                            ValidationError validationError = new ValidationError();
                            Map<String, Integer> map = new HashMap<>();
                            map.put(MIN_STRING, annotation.min());
                            map.put(MAX_STRING, annotation.max());
                            validationError.setFieldName(field.getName());
                            validationError.setFieldType(valueProvider.getType());
                            validationError.setErrorMessage(StringFormatService.format(annotation.message(), PATTERN, map));
                            errors.add(validationError);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    public void processErrors() {
        for (ValidationError validationError : errors) {
            String errorMessage = validationError.getFieldName();
            errorMessage += validationError.getFieldType();
            errorMessage += validationError.getErrorMessage();
            ValidatorController.updateView(errorMessage);
        }
    }
}
