package net.therap.service.validator;

import net.therap.model.Height;
import net.therap.model.ValueProvider;

/**
 * @author al.imran
 * @since 28/03/2021
 */
public class HeightValueProvider implements ValueProvider<Height> {
    private static final String VALUE_TYPE = "(Height): ";

    @Override
    public int getSize(Height object) {
        return object.getHeight();
    }

    @Override
    public String getType() {
        return VALUE_TYPE;
    }
}
