package net.therap.service.validator;

import net.therap.model.ValueProvider;
import java.util.Collection;

/**
 * @author al.imran
 * @since 28/03/2021
 */
public class CollectionValueProvider implements ValueProvider<Collection> {
    private static final String VALUE_TYPE = "(Collection): ";

    @Override
    public int getSize(Collection object) {
        return object.size();
    }

    @Override
    public String getType() {
        return VALUE_TYPE;
    }
}
