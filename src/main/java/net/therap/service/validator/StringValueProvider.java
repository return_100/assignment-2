package net.therap.service.validator;

import net.therap.model.ValueProvider;

/**
 * @author al.imran
 * @since 28/03/2021
 */
public class StringValueProvider implements ValueProvider<String> {
    private static final String VALUE_TYPE = "(String): ";

    @Override
    public int getSize(String object) {
        return object.length();
    }

    @Override
    public String getType() {
        return VALUE_TYPE;
    }
}
