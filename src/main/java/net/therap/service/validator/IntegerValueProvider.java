package net.therap.service.validator;

import net.therap.model.ValueProvider;

/**
 * @author al.imran
 * @since 28/03/2021
 */
public class IntegerValueProvider implements ValueProvider<Integer> {
    private static final String VALUE_TYPE = "(int): ";

    @Override
    public int getSize(Integer object) {
        return (int) object;
    }

    @Override
    public String getType() {
        return VALUE_TYPE;
    }
}
