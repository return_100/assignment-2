package net.therap.service.string;

import java.util.Map;
import java.util.regex.*;

/**
 * @author al.imran
 * @since 27/03/2021
 */
public class StringFormatService {
    public static String format(String message, String pattern, Map<String, Integer> map) {
        String formattedMessage = message;
        Pattern pat = Pattern.compile(pattern);
        Matcher matcher = pat.matcher(message);
            String key;
            while (matcher.find()) {
                key = matcher.group();
                if (map.containsKey(key)) {
                    formattedMessage = formattedMessage.replace(key, map.get(key).toString());
                }
            }
        return formattedMessage;
    }
}
