package net.therap;

import net.therap.controller.ValidatorController;
import net.therap.model.Person;
import net.therap.service.validator.ValidatorService;

/**
 * @author al.imran
 * @since 23/03/2021
 */
public class AnnotatedValidator {
    public static void main(String[] args) {
        ValidatorService validatorService = new ValidatorService();
        ValidatorController validatorController = new ValidatorController(validatorService);
        Person infant = new Person("Error Error Error Error", 1, 7);
        Person child = new Person("underage", 3, 4);
        Person boy = new Person("it is not a valid name", 19, 5);
        Person man = new Person("man", 26, 6);
        man.populateList();
        validatorController.validate(infant);
        validatorController.validate(child);
        validatorController.validate(boy);
        validatorController.validate(man);
        validatorController.processErrors();
    }
}
