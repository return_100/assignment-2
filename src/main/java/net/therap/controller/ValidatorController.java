package net.therap.controller;

import net.therap.model.Person;
import net.therap.service.validator.ValidatorService;
import net.therap.view.ValidatorView;

/**
 * @author al.imran
 * @since 23/03/2021
 */
public class ValidatorController {
    private final ValidatorService validatorService;

    public ValidatorController(ValidatorService validatorService) {
        this.validatorService = validatorService;
    }

    public void validate(Person person) {
        validatorService.validate(person);
    }

    public void processErrors() {
        validatorService.processErrors();
    }

    public static void updateView(String errorMessage) {
        ValidatorView.viewErrorMessage(errorMessage);
    }
}
